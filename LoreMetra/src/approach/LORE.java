package approach;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import expansion.RocchioHelper;
import nlp.HumanProcessing;
import nlp.Lemmatizer;
import nlp.POSTagger;
import nlp.Tokenizer;
import nlp.Lowercaser;
import ontology.OntologyConcept;
import ontology.OntologyHelper;

/**
 * Linguistic Ontological Requirements Expansion (LORE) Approach
 * 
 * Takes a Requirement and an Ontology and performs the following steps:
 * 	1. Requirement NLP - Extracts the Requirement term lists
 *  2. Ontology NLP - Performs NLP on the Ontology to unify the language
 *  3. Calculates the Ontological Affinity Documents (OADs) for the Requirement
 *  4. Expands the Requirement through Rocchio's query expansion
 * 
 * Returns an expanded Requirement
 * 
 * @author Ra�l
 */
public class LORE {

	OntologyHelper oh;
	
	ArrayList<String> domainTerms;
	ArrayList<String> stopwords;
	
	private POSTagger pt;
	private Lemmatizer lm;
	
	public LORE(ArrayList<String> domainTerms, ArrayList<String> stopwords, POSTagger pt, Lemmatizer lm) {
		this.domainTerms = domainTerms;
		this.stopwords = stopwords;
		this.oh = new OntologyHelper();
		this.pt = pt;
		this.lm = lm;
		
		// Process the ontology
		processOntology();
	}
	
	/**
	 * Comprises the whole query expansion process
	 * @return
	 */
	public ArrayList<String> run(String query) {
		
		// Extract the query term lists
		ArrayList<ArrayList<String>> termLists = extractTermLists(query);
		
		// Calculate the OADs for the requirement
		ArrayList<HashMap<String,Integer>> oads = generateOADs(termLists);
		
		// Build the query from the term lists
		ArrayList<String> expandedQuery = new ArrayList<String>();
		for(ArrayList<String> terms : termLists){
			expandedQuery.addAll(terms);
		}
		
		// Expand the requirement using the OAD
		RocchioHelper rocchio = new RocchioHelper(expandedQuery, oads, oads, 10);
		ArrayList<String> rocchioQuery = rocchio.run();
		return rocchioQuery;		
	}
	
	/**
	 * Method that extracts the Requirement Term Lists through NLP
	 * @param requirement
	 */
	private ArrayList<ArrayList<String>> extractTermLists(String query) {
		
		// Create the terms lists
		ArrayList<ArrayList<String>> termLists = new ArrayList<ArrayList<String>>();
		
		// Split the requirement into phrases
		ArrayList<String> phrases = Tokenizer.dotTokenizer(query);
		
		// Calculate the terms for each phrase
		for (String phrase : phrases){
			
			// List of terms for the phrase
			ArrayList<String> terms = new ArrayList<String>();
			
			// Add the appearing domain terms
			terms.addAll(HumanProcessing.extractDomainTerms(phrase, domainTerms));
			
			// Tokenize the phrase through whitespace into words & lowercase them 
			ArrayList<String> words = Tokenizer.whitespaceTokenizer(phrase);
			words = Lowercaser.process(words);
			
			// POS Tag the words
			ArrayList<String> tags = pt.process(words);
			
			// Lemmatize the words
			ArrayList<String> lemmas = lm.process(words, tags);
			
			// Filter the lemmas through nouns
			ArrayList<String> nouns = pt.filterNouns(lemmas, tags);
			
			// Remove the stopwords
			ArrayList<String> filteredNouns = HumanProcessing.removeStopwords(nouns, stopwords);
			
			// Add the remaining nouns to the list of terms
			terms.addAll(filteredNouns);
						
			// Add the term list to the collection of term lists
			termLists.add(terms);
			
		}
		
		// Return the collection of term lists
		return termLists;
		
	}
	
	/**
	 * Method that performs the NLP process on the ontology
	 */
	private void processOntology() {
		
		// Get the ontology
		ArrayList<OntologyConcept> ontology = oh.getOntology();
		
		// For every concept in the ontology
		for (OntologyConcept concept : ontology) {
				
			// Get the words of the concept
			ArrayList<String> conceptWords = Tokenizer.whitespaceTokenizer(concept.getTerm());
			
			// Create the tags for the words (all words considered nouns)
			ArrayList<String> tags = new ArrayList<String>();
			for(@SuppressWarnings("unused") String word : conceptWords) tags.add("NN");
			
			// Get the lemmas of the concept words
			ArrayList<String> lemmas = lm.process(conceptWords, tags);
			
			// Build the lemmatized concept
			String lemmatizedConcept = "";
			for(String lemma : lemmas)
				lemmatizedConcept = lemmatizedConcept + " " + lemma;
			lemmatizedConcept = lemmatizedConcept.trim();
			
			// Set the lemmatized concept
			concept.setTerm(lemmatizedConcept);
			
		}
		
	}

	/**
	 * Method that generates the OADs for each list of terms
	 * @param termLists The term lists for which OADs must be generated
	 * @return An ArrayList of OADs (HashMap<String,Integer>)
	 */
	private ArrayList<HashMap<String, Integer>> generateOADs(ArrayList<ArrayList<String>> termLists){
		
		// Create oads
		ArrayList<HashMap<String, Integer>> oads = new ArrayList<HashMap<String, Integer>>();
		
		// For each term list
		for(ArrayList<String> termList : termLists) {
			
			// Retrieve OAD
			ArrayList<String> oad = oh.generateOAD(termList);
			
			// Convert it to hashmap of unique values with term frequency
			HashMap<String, Integer> oadAsMap = new HashMap<String, Integer>();
			for(String term : oad) {
				if(!oadAsMap.containsKey(term))
					oadAsMap.put(term, Collections.frequency(oad, term));
			}
			
			// Add OAD to OADs
			oads.add(oadAsMap);				
		}
		
		return oads;
		
	}
	
}
 