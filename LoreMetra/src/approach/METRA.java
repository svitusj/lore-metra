package main;

import java.util.ArrayList;
import java.util.Collections;

import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.xml.instance.ModelElementInstance;

import classes.CustomModelElement;
import resources.Constants;
import utils.BpmnUtil;
import utils.LSIUtil;
import utils.LinguisticsUtil;

public class METRA 
{
	public METRA(ArrayList<BpmnModelInstance> modelFragments, ArrayList<String> requirements) 
	{
		
		
		// Create the LSI Util
		LSIUtil lsi = new LSIUtil();
		
		int index = 0;
		int dimension = modelFragments.size() - 4;
		
		// For each requirement, do LSI
		for(String requirement : requirements)
		{
			ArrayList<CustomBPMNModelInstance> simElements = new ArrayList<CustomBPMNModelInstance>();
			simElements.addAll(lsi.PerformLSI(modelFragments, requirement));
			
			Collections.sort(simElements);
			
			for(CustomBPMNModelInstance fragment : simElements)
			{
				if(fragment.similitude >= 0.7)
				System.out.println("Req: " + requirement + "fragment id: " + fragment.fragment.getAttributeValue("id") + ", sim value: " + fragment.similitude);
			}
			
			System.out.println("\n");
		}
		
		
		

	}

}
