package expansion;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class RocchioHelper {

	
	private ArrayList<String> queryKeywords;
	private ArrayList<HashMap<String, Integer>> relevantDocs;
	private ArrayList<HashMap<String, Integer>> corpusDocs;
	private final int termsToExpand;
	
	
	public RocchioHelper(ArrayList<String> queryKeywords,ArrayList<HashMap<String, Integer>> relevantDocs, ArrayList<HashMap<String, Integer>> corpusDocs, int termsToExpand)
	{
		this.queryKeywords=new ArrayList<String>(queryKeywords);
		this.corpusDocs=new ArrayList<HashMap<String, Integer>>(corpusDocs);
		this.relevantDocs=new ArrayList<HashMap<String, Integer>>(relevantDocs);
		this.termsToExpand=termsToExpand;
		
	}
	
	public ArrayList<String> run()
	{
		ArrayList<String> result=new ArrayList<String>(queryKeywords);
		
		ArrayList<HashMap<String, Double>> resultTfxIdfValuesInDocs=getTfxIdfValues(relevantDocs, corpusDocs);
		
		HashMap <String, Double> resultWeights=new HashMap<String, Double>();
//		if(Logger.WHO_QUERY_REFORMULATION_ENABLED) Logger.log(Logger.WHO_QUERY_REFORMULATOR,getClass().getCanonicalName(),Logger.ACTION_LOG, getNumDocsHasAWord(corpusDocs, "panto"));
		
		for (HashMap<String,Double> doc: resultTfxIdfValuesInDocs)
		{
						
			for (HashMap.Entry<String, Double> entry:doc.entrySet())
			{
				String word = entry.getKey();
			    double tfxIdfValue = entry.getValue();
			    
			    if(resultWeights.containsKey(word)) resultWeights.replace(word, resultWeights.get(word)+tfxIdfValue);
			    else resultWeights.put(word, (double) tfxIdfValue);
			}
		}
		
	
		Map<String, Double> sortedTermWeights=sortByComparator(resultWeights, false);
		//resultWeights.

		//printSortedTermWeights(sortedTermWeights);
		
		result = expandQuery(termsToExpand, result, sortedTermWeights);
		
		return result;
	}
	
	

	private ArrayList<String> expandQuery(int termsToExpand, ArrayList<String> result,
			Map<String, Double> sortedTermWeights) {
		for (Entry<String, Double> entry : sortedTermWeights.entrySet())
		{
			termsToExpand--;
			String word=entry.getKey();
			if(termsToExpand<0) break;
			else if(!result.contains(word) && entry.getValue()>0) result.add(word);
			//value>0 to ensure that an expanded term has some weight
			
		}
		return result;
	}
	
	
	private int getNumDocsHasAWord(ArrayList<HashMap<String, Integer>> documents, String word)
	{
		int result=0;
		//number of times that a term appears in a document
		for (HashMap<String,Integer> doc: documents)
		{
			if(doc.containsKey(word)) result++;
		}
	
		return result;
	}
	
	
	private ArrayList<HashMap<String, Double>> getTfxIdfValues(ArrayList<HashMap<String, Integer>> relevantDocs, ArrayList<HashMap<String, Integer>> corpusDocs)
	{
		ArrayList<HashMap<String, Double>> resultTfxIdfValuesInDocs=new ArrayList<HashMap<String, Double>>();
		for (HashMap<String,Integer> doc: relevantDocs)
		{
			HashMap<String,Double> resultDoc= new HashMap<String,Double> ();
			
			for (HashMap.Entry<String, Integer> entry:doc.entrySet())
			{
				String word = entry.getKey();
			    int frequencyInDoc = entry.getValue();
			    int numCorpusDocsHasTheWord=getNumDocsHasAWord(corpusDocs, word);
			    double idf=1.0/numCorpusDocsHasTheWord;
			    double value=frequencyInDoc*idf;
			    
			    resultDoc.put(word, value);
			}
			resultTfxIdfValuesInDocs.add(resultDoc);
		
	}
	return resultTfxIdfValuesInDocs;
}
private static Map<String, Double> sortByComparator(Map<String, Double> unsortMap, final boolean order)
{
	//order attribute
	//	public static boolean ASC = true;
	//  public static boolean DESC = false;

    List<Entry<String, Double>> list = new LinkedList<Entry<String, Double>>(unsortMap.entrySet());

    // Sorting the list based on values
    Collections.sort(list, new Comparator<Entry<String, Double>>()
    {
        public int compare(Entry<String, Double> o1,
                Entry<String, Double> o2)
        {
            if (order)
            {
                return o1.getValue().compareTo(o2.getValue());
            }
            else
            {
                return o2.getValue().compareTo(o1.getValue());

            }
        }
    });

    // Maintaining insertion order with the help of LinkedList
    Map<String, Double> sortedMap = new LinkedHashMap<String, Double>();
    for (Entry<String, Double> entry : list)
    {
        sortedMap.put(entry.getKey(), entry.getValue());
    }

    return sortedMap;
}


}
