package utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import opennlp.tools.tokenize.WhitespaceTokenizer;

import org.camunda.bpm.model.xml.instance.ModelElementInstance;
import org.ejml.alg.dense.mult.VectorVectorMult;
import org.ejml.simple.SimpleMatrix;
import org.ejml.simple.SimpleSVD;

import classes.CustomModelElement;

/**
 * Class used to perform the LSI process
 * @author Ra�l
 */
public class LSIUtil 
{
	/* Variables for the LSI Term-by-Document Co-Occurrence Matrix and Query Matrix */
	private SimpleMatrix termByDocumentCoOccurrenceMatrix;
	private SimpleMatrix queryMatrix;
	
	public LSIUtil()
	{
		termByDocumentCoOccurrenceMatrix = new SimpleMatrix();
		queryMatrix = new SimpleMatrix();
	}
	
	
	/* LSI Methods -> Build matrix, build query, calculate LSI similitude values, produce ranking */
	
	/**
	 * Method that builds the Term-by-Document Co-Occurrence Matrix counting occurrences of the terms in the elements.
	 * @param terms Keywords of the problem
	 * @param elements List of model elements
	 */
	private void buildTermByDocumentCoOccurrenceMatrix(List<String> terms, List<ModelElementInstance> elements)
	{
		/* What needs to be done is count the term occurrences in each document */
		/* For each term, count the occurrences in an equipment, then go to the next document */
		// Start at row 0 (first term)
		int rowControl = 0;

		// The matrix has to have dimensions [number of keywords][number of documents]
		double[][] termByDocumentCoOccurrenceMatrixData = new double[terms.size()][elements.size()];
		
		// For each term in the list of terms
		for(String term : terms)
		{
			// Restart the column count at the beginning (we are at the beginning of a term calculations, we have to go through all the documents)
			int colControl = 0;
			
	
			// For each document in the list of processed documents
			for(ModelElementInstance element : elements)
			{
				// Count the number of times that the term appears in the element
				int ocurrences = 0;
				Pattern p = Pattern.compile(term);
				Matcher m = p.matcher(element.getAttributeValue("name"));
				while (m.find()) {
				    ocurrences++;
				}
				
				// Set the value on the matrix
				termByDocumentCoOccurrenceMatrixData[rowControl][colControl] = ocurrences;
				
				// Increase the column (we go to the next document)
				colControl++;
			}
			
			// Once the term occurrences have been calculated for all the documents, go to the next row (next term)
			rowControl++;
		}
		
		// When all values have been calculated, then fill the matrix data
		termByDocumentCoOccurrenceMatrix = new SimpleMatrix(termByDocumentCoOccurrenceMatrixData);
	}
	
	/**
	 * Method that builds the Query matrix counting occurrences of the terms in the requirement.
	 * @param terms Keywords of the problem
	 * @param query The query requirement
	 */
	private void buildQueryMatrix(List<String> terms, String query)
	{
		/* What needs to be done is count term occurrences in the query for each term */

		// Start at row 0 (first term)
		int rowControl = 0;
		
		// The query is just one column
		double[][] queryMatrixData = new double[terms.size()][1];
		
		// For each term in the list of terms
		for(String term : terms)
		{
			// Count the occurrences of the term in the query
			int ocurrences = 0;
			Pattern p = Pattern.compile(term);
			Matcher m = p.matcher(query);
			while (m.find()) {
			    ocurrences++;
			}
			
			// Set the value on the matrix
			queryMatrixData[rowControl][0] = ocurrences;

			// Once the term occurrences have been calculated for the query, go to the next row (next term)
			rowControl++;
		}
		
		// When all values have been calculated, then fill the matrix data
		queryMatrix = new SimpleMatrix(queryMatrixData);
	}

	/**
	 * Method that calculates the LSI similitude values between the documents and the query.
	 * @param equipments The equipments
	 * @param query The query
	 * @param dimension Dimension to which the matrices of the LSI must be reduced
	 */
	@SuppressWarnings("rawtypes")
	private ArrayList<CustomModelElement> calculateLSISimilitudeValues(List<ModelElementInstance> elements, int dimension)
	{
		// Create list of CustomModelElement
		ArrayList<CustomModelElement> simElements = new ArrayList<CustomModelElement>();
	
		// Get the SVD decomposition of the Term-by-Document Co-Occurrence Matrix
		SimpleSVD matrixSVD = termByDocumentCoOccurrenceMatrix.svd(true);
				
		// Get the three matrices that compose the SVD
		SimpleMatrix u = matrixSVD.getU();
		SimpleMatrix w = matrixSVD.getW();
		SimpleMatrix v = matrixSVD.getV();
		
		/* Reduce the matrices to work with a certain number of dimensions */
		SimpleMatrix reducedU = new SimpleMatrix(u.numRows(),dimension);
		SimpleMatrix reducedW = new SimpleMatrix(dimension,dimension);
		SimpleMatrix reducedV = new SimpleMatrix(v.numRows(),dimension);
		
		/* Fill the reduced matrices */
		// Fill reducedU
		for(int row = 0; row < u.numRows(); row++)
		{
			for(int col = 0; col < dimension; col++)
				reducedU.set(row, col, u.get(row,col));
		}
		// Fill reducedW
		for(int row = 0; row < dimension; row++)
		{
			for(int col = 0; col < dimension; col++)
				reducedW.set(row, col, w.get(row,col));
		}
		// Fill reducedV
		for(int row = 0; row < v.numRows(); row++)
		{
			for(int col = 0; col < dimension; col++)
				reducedV.set(row, col, v.get(row,col));
		}
					
		// The vectors of the documents are stored in the V matrix rows
		// Create a list of vectors and add the V matrix rows recursively
		List<SimpleMatrix> documentVectors = new ArrayList<SimpleMatrix>();
		for(int i = 0; i < reducedV.numRows(); i++)
		{
			documentVectors.add(reducedV.extractVector(true, i));
		}
	
		// Obtain the vector associated to the query
		SimpleMatrix transposedQuery = queryMatrix.transpose();
		SimpleMatrix invertedW = reducedW.invert();
		SimpleMatrix queryVector = transposedQuery.mult(reducedU).mult(invertedW);
		
		// Calculate the cosine similitude for each of the documents
		int documentIndex = 0;
		for(SimpleMatrix vector : documentVectors)
		{
			// Calculate the dot product between the document vector and the query vector, and the norms of both vectors
			double dotProd = VectorVectorMult.innerProd(queryVector.getMatrix(), vector.getMatrix());
			double queryNorm = queryVector.normF();
			double vectorNorm = vector.normF();
			
			// Perform the similitude operation
			double similitude = dotProd / (queryNorm * vectorNorm);
			
			// Generate the model element and add it to the returned list
			CustomModelElement simElement = new CustomModelElement(elements.get(documentIndex), similitude);
			simElements.add(simElement);
				
			documentIndex++;
		}
		
		return simElements;
	}
	
	/**
	 * Static method that can be called to retrieve the similitude values of LSI for a collection of documents + query with a certain dimension reduction.
	 * @param elements The elements of the problem model
	 * @param query The query of the problem
	 * @param dimension The dimension to which the matrices of the LSI must be reduced
	 */
	public ArrayList<CustomModelElement> PerformLSI(List<ModelElementInstance> elements, String query, int dimension)
	{
		// Create list of CustomModelElement
	    ArrayList<CustomModelElement> simElements = new ArrayList<CustomModelElement>();
				
		// Extract the keywords from the collection, removing duplicates
		List<String> keywords = extractKeywords(elements, query);
		
		// Build the Term-by-Document Co-Ocurrence Matrix
		buildTermByDocumentCoOccurrenceMatrix(keywords, elements);
		
		// Build the Query Matrix
		buildQueryMatrix(keywords, query);
		
		// Calculate the similitude values between documents and query
		simElements.addAll(calculateLSISimilitudeValues(elements, dimension));
		
		return simElements;
	}
	
	/* Auxiliary methods */
	
	/**
	 * Method used to retrieve the keywords that must be used for the LSI process
	 * @param elements List of model elements
	 * @param query Query
	 * @return
	 */
	private List<String> extractKeywords(List<ModelElementInstance> elements, String query)
	{
		List<String> keywords = new ArrayList<String>();

		// Retrieve the words in the elements
		for(ModelElementInstance element : elements)
		{
			// Tokenize the element string
	        String[] wordsArray = WhitespaceTokenizer.INSTANCE.tokenize(element.getAttributeValue("name"));
	        
	        // Add the words to the keywords list 
	        for(String word : wordsArray)
	        	keywords.add(word);
	    }
		
		// Retrieve the words in the query
		// Tokenize the query string
        String[] wordsArray = WhitespaceTokenizer.INSTANCE.tokenize(query);
        
        // Add the words to the keywords list 
        for(String word : wordsArray)
        	keywords.add(word);
		
		// Remove duplicate keywords
		removeDuplicateWords(keywords);
		
		// Sort the keywords alphabetically
		Collections.sort(keywords);
		
		return keywords;
	}
	
	/**
	 * Method that removes duplicates from a list of words
	 * @param words The list of words from which duplicates must be removed
	 */
	public void removeDuplicateWords(List<String> words)
	{
		// Create a Set -> Sets don't allow duplicates!
		// Move the words to the set and back to the ArrayList
		Set<String> wordSet = new HashSet<>();
		wordSet.addAll(words);
		words.clear();
		words.addAll(wordSet);
	}
	
}
