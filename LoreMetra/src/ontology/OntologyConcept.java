package ontology;

import java.util.ArrayList;

public class OntologyConcept {
	
	private String term;
	private ArrayList<OntologyConcept> relatedConcepts;
	
	/* Constructor */
	
	public OntologyConcept(String term) {
		this.term = term;
		this.relatedConcepts = new ArrayList<OntologyConcept>();
	}
	
	public OntologyConcept(String term, ArrayList<OntologyConcept> relatedConcepts){
		this.term = term;
		this.relatedConcepts = relatedConcepts;
	}
	
	/* Methods */
	
	/**
	 * Method that adds a bidirectional relationship between concepts
	 * @param concept The concept that relates to the concept from which the method is invoked
	 */
	public void addRelatedConcept(OntologyConcept concept) {
		
		this.relatedConcepts.add(concept);
		concept.getRelatedConcepts().add(this);
		
	}
	
	/**
	 * Method that adds several bidirectional relationships between concepts
	 * @param concepts A collection of concepts that relate to the concept from which the metod is invoked
	 */
	public void addRelatedConcepts(ArrayList<OntologyConcept> concepts) {
		
		for(OntologyConcept concept : concepts) 
			addRelatedConcept(concept);
		
	}
		
	/* Setters & Getters */

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public ArrayList<OntologyConcept> getRelatedConcepts() {
		return relatedConcepts;
	}

	public void setRelatedConcepts(ArrayList<OntologyConcept> relatedConcepts) {
		this.relatedConcepts = relatedConcepts;
	}
	
	/* toString */
	
	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder("Ontology Concept\r\n");
		sb.append("\tTerm: " + term + "\r\n");
		
		for(OntologyConcept concept : relatedConcepts) sb.append("\t\tRelated concept: " + concept.term + "\r\n");
		
		return sb.toString();
	}

}
