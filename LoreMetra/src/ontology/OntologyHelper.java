package ontology;

import java.util.ArrayList;

public class OntologyHelper {
	
	ArrayList<OntologyConcept> ontology;
	
	/* Constructor */
	
	public OntologyHelper(){
		this.ontology = loadOntology();
	}
	
	/* Ontology Loading */
	
	public ArrayList<OntologyConcept> loadOntology(){
		
		// Create the ontology
		ArrayList<OntologyConcept> ontology = new ArrayList<OntologyConcept>();
		
		// Create the concepts as per the examples
		// OntologyConcept pantograph = new OntologyConcept("pantograph");
		// OntologyConcept brake = new OntologyConcept("brake");
		
		// Add the concept relationships as per the example
		// pantograph.addRelatedConcept(brake);
		
		// Add the concepts to the ontology as per the example
		// ontology.add(pantograph);
		// ontology.add(brake);
			
		// Return the ontology
		return ontology;
	}
		
	/* Useful Ontology Methods */
	
	/**
	 * Method that generates the Ontological Affinity Document (OAD) for a particular list of terms 
	 * @param terms The terms for which the OAD must be retrieved
	 * @return The OAD for the introduced terms as ArrayList of String
	 */
	public ArrayList<String> generateOAD(ArrayList<String> terms){
		
		// Create OAD
		ArrayList<String> oad = new ArrayList<String>();
		
		// For each term in the list of terms
		for(String term : terms){
			
			// Find the term in the ontology
			OntologyConcept oc = findConceptByTerm(term);
			
			// If the term is found
			if(oc != null){
				
				// Add all the concepts related to the term
				for(OntologyConcept relatedConcept : oc.getRelatedConcepts())
					oad.add(relatedConcept.getTerm());
				
			}
		}
		
		return oad;
	}
	
	/**
	 * Method that retrieves an ontology concept for a certain term.
	 * @param term The term for which its corresponding ontology concept must be retrieved
	 * @return The ontology concept that corresponds to the term (null if the term is not found in the ontology).
	 */
	private OntologyConcept findConceptByTerm(String term){
		
		// Retrieve the concept in the ontology
		for(OntologyConcept concept : ontology) 
			if(concept.getTerm().equals(term)) return concept;
		
		// Return null if not found
		return null;
	}
	
	/* Setters & Getters */

	public ArrayList<OntologyConcept> getOntology() {
		return ontology;
	}

	public void setOntology(ArrayList<OntologyConcept> ontology) {
		this.ontology = ontology;
	}

	/* toString */

	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder("Ontology\r\n\n");
		
		for(OntologyConcept concept : ontology) sb.append(concept.toString());

		return sb.toString();
	}
	
	

}
