package utils;

import java.io.InputStream;

import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;

public class ModelUtil {
	
	/**
	 * Method that loads a BPMN model from a given path
	 * @param path The path from which the BPMN model must be loaded
	 * @return The loaded BPMN model
	 */
	public static BpmnModelInstance loadModelFromPath(String path) {
		
		// Create an input stream, read the model, and return it
		InputStream is = ModelUtil.class.getResourceAsStream(path);
		BpmnModelInstance modelInstance = Bpmn.readModelFromStream(is);
		return modelInstance;
		
	}
	
}