package nlp;

import java.util.ArrayList;

public class Tokenizer {
	
	/**
	 * Method that splits a text into sub-sentences using whitespace as separator
	 * @param text The text that must be split
	 * @return The words of the text as collection
	 */
	public static ArrayList<String> whitespaceTokenizer(String text) {
		
		ArrayList<String> processedText = new ArrayList<String>();
		
		String[] words = text.split(" ");
		
		for(String word : words) processedText.add(word);
		
		return processedText;
	}
	
	/**
	 * Method that splits a text into sub-sentences using dot as separator
	 * @param text The text that must be split
	 * @return The words of the text as collection
	 */
	public static ArrayList<String> dotTokenizer(String text) {
		
		ArrayList<String> processedText = new ArrayList<String>();
		
		String[] words = text.split("\\.");
		
		for(String word : words) processedText.add(word);
		
		return processedText;
	}
}
