package nlp;

import java.util.ArrayList;

public class Lowercaser {

	/**
	 * Method that lowercases a single word
	 * @param word The word to be lowercased
	 * @return The lowercased word
	 */
	public static String process(String word) {	
		return word.toLowerCase();
	}
	
	/**
	 * Method that lowercases a collection of words
	 * @param words The words to be lowercased
	 * @return A collection containing the original words, lowercased
	 */
	public static ArrayList<String> process(ArrayList<String> words) {
		ArrayList<String> processedWords = new ArrayList<String>();
		for (String word : words) processedWords.add(word.toLowerCase());
		return processedWords;
	}

}
