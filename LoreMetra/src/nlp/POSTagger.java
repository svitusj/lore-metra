package nlp;

import java.io.InputStream;
import java.util.ArrayList;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;

public class POSTagger {

	private final String posModelRoute = "/resources/en-pos-maxent.bin";
	private POSTaggerME pt;
	
	public POSTagger() {
		try {
			
			// Get the model and tagger
			InputStream is = POSTagger.class.getResourceAsStream(posModelRoute);
			POSModel model = new POSModel(is);
			this.pt = new POSTaggerME(model);
			is.close();
		
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Method that POS tags a collection of words
	 * @param words The words that must be tagged
	 * @return The retrieved collection of tags (null if there is an exception)
	 */
	public ArrayList<String> process(ArrayList<String> words) {
		
		try {
			
			// Words as string
			String[] wordsAsArray = new String[words.size()];
			for(int i = 0; i < words.size(); i++) wordsAsArray[i] = words.get(i);
			
			// Retrieve & return the tags as list
			String[] tagsAsArray = pt.tag(wordsAsArray);
			ArrayList<String> tagsAsList = new ArrayList<String>();
			for(String tag : tagsAsArray) tagsAsList.add(tag);

			return tagsAsList;
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return null;
		
		}
		
	}
	
	/**
	 * Method that retrieves the nouns from a list of tagged words 
	 * @param words The words from which nouns must be retrieved
	 * @param tags The list of tags that correspond to the words
	 * @return The nouns as a collection
	 */
	public ArrayList<String> filterNouns(ArrayList<String> words, ArrayList<String> tags) {
		
		ArrayList<String> filteredWords = new ArrayList<String>();
		
		for(int i = 0 ; i < words.size() ; i++)
			if(tags.get(i).contains("NN"))
				filteredWords.add(words.get(i));
		
		return filteredWords;
	}
	
}
