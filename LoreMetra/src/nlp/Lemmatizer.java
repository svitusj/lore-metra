package nlp;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import opennlp.tools.lemmatizer.DictionaryLemmatizer;

public class Lemmatizer {

	private final String dictRoute = "/resources/en-lemmatizer.dict";
	private DictionaryLemmatizer lemmatizer;
	
	public Lemmatizer() {
		try{
			
			// Get the lemmatizer
			InputStream is = Lemmatizer.class.getResourceAsStream(dictRoute);
			this.lemmatizer = new DictionaryLemmatizer(is);
			is.close();
		
		}catch(Exception e){
			
			e.printStackTrace();
		
		}
	}
	
	/**
	 * Method that lemmatizes a collection of words according to their POS tags
	 * @param words The words that must be lemmatized
	 * @param tags The POS Tags of said words
	 * @return 	The retrieved collection of lemmas (null if there is an exception)
	 */
	public ArrayList<String> process(ArrayList<String> words, ArrayList<String> tags) {

		try {
			
			// Convert to arrays
			String[] wordsAsArray = words.toArray(new String[0]);
			String[] tagsAsArray = tags.toArray(new String[0]);
			
			// Retrieve & return the lemmas
			String[] lemmasAsArray = lemmatizer.lemmatize(wordsAsArray, tagsAsArray);
			ArrayList<String> lemmasAsList = new ArrayList<String>(Arrays.asList(lemmasAsArray));
			return lemmasAsList;
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return null;
		
		}

	}
	
}
