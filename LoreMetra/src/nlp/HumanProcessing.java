package nlp;

import java.util.ArrayList;

public class HumanProcessing {
	
	/**
	 * Method that extracts the domain terms from a text
	 * @param text The text from which domain terms must be extracted
	 * @param domainTerms The domain terms
	 * @return The retrieved collection of domain terms
	 */
	public static ArrayList<String> extractDomainTerms(String text, ArrayList<String> domainTerms) {
		
		ArrayList<String> presentDomainTerms = new ArrayList<String>();
		
		for(String domainTerm : domainTerms)
			if (text.contains(domainTerm))
				presentDomainTerms.add(domainTerm);
		
		return presentDomainTerms;
		
	}
	
	/**
	 * Method that removes the stopwords from a collection of words
	 * @param text The words from which stopwords must be removed
	 * @param stopwords The stopwords provided by engineers
	 * @return The collection of words without the stopwords
	 */
	public static ArrayList<String> removeStopwords(ArrayList<String> words, ArrayList<String> stopwords) {
		
		ArrayList<String> remainingWords = words;
		remainingWords.removeAll(stopwords);
		return remainingWords;
		
	}

}
